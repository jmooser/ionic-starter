# Ionic-Starter 
## Created and maintained by Justin Mooser using Yeomo's ionic-generator project.

## Steps to reproduce: 

Install the generator: 
`npm install -g generator-ionic`

Make a new directory, and cd into it:
`mkdir my-ionic-project && cd $_`

Run yo ionic:
`yo ionic`

Install compass: 
`npm install grunt-contrib-compass --save-dev`

Assuming you have Ruby installed: 
`gem update --system && gem install compass`

Add to Gruntfile.js:
grunt.loadNpmTasks('grunt-contrib-compass');
grunt.registerTask('default', ['jshint', 'compass']);  — the important part being the addition of ‘compass'


## How to use: 
Managing libraries with Bower

Install a new front-end library using bower install --save to update your bower.json file.

`bower install --save lodash`
This way, when the Grunt bower-install task is run it will automatically inject your front-end dependencies inside the bower:js block of your app/index.html file.


Manually adding libraries

If a library you wish to include is not registered with Bower or you wish to manually manage third party libraries, simply include any CSS and JavaScript files you need inside your app/index.html usemin build:js or build:css blocks but outside the bower:js or bower:css blocks (since the Grunt task overwrites the Bower blocks' contents).

##To Develop: 

Just type:
`grunt serve` 

To get things working in the browser.  Note that it will not bundle/minify if you just use grunt serve.  To ensure nothing gets screwed up in the build process run: 
`grunt serve:dist` this will run the full pipeline.

## Cordova Specifics: 

Building assets for Cordova

Once you're ready to test your application in a simulator or device, run `grunt cordova` to copy all of your app/ assets into www/ and build updated platform/ files so they are ready to be emulated / run by Cordova.

To compress and optimize your application, run grunt build. It will concatenate, obfuscate, and minify your JavaScript, HTML, and CSS files and copy over the resulting assets into the www/ directory so the compressed version can be used with Cordova.

## Cordova commands

To make our lives a bit simpler, the cordova library has been packaged as a part of this generator and delegated via Grunt tasks. To invoke Cordova, simply run the command you would normally have, but replace cordova with grunt and spaces with : (the way Grunt chains task arguments).

For example, lets say you want to add iOS as a platform target for your Ionic app

`grunt platform:add:ios`
and emulate a platform target

`grunt emulate:ios`
or add a plugin by specifying either its full repository URL or namespace from the Plugins Registry

`grunt plugin:add:https://git-wip-us.apache.org/repos/asf/cordova-plugin-device.git`
`grunt plugin:add:org.apache.cordova.device`
`grunt plugin:add:org.apache.cordova.network-information`

